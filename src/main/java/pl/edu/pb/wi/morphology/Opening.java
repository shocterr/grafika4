package pl.edu.pb.wi.morphology;

import java.awt.image.BufferedImage;

public abstract class Opening {

    public static BufferedImage opening(BufferedImage image){
        BufferedImage imgCopy = Erosion.erosion(image);
        return Dilatation.dilatate(imgCopy);
    }

}
