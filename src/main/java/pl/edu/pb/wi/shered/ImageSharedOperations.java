package pl.edu.pb.wi.shered;

import pl.edu.pb.wi.filtration.SimpleFilter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static pl.edu.pb.wi.filtration.SimpleFilter.deepCopy;

public class ImageSharedOperations {

    public static final int CHANNEL_RED = 0;
    public static final int CHANNEL_GREEN = 1;
    public static final int CHANNEL_BLUE = 2;
    public static final int CHANNEL_COMBINED = 3;
    private static int[] LOOKUP_TABLE_RED = new int[256];
    private static int[] LOOKUP_TABLE_GREEN = new int[256];
    private static int[] LOOKUP_TABLE_BLUE = new int[256];

    public static BufferedImage loadImage(String path) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(path));
        } catch (IOException ex) {
            System.out.println("Error has occured during file reading: " + ex.getMessage());
        }
        return image;
    }

    public static void saveImage(BufferedImage img, String path) {
        try {
            ImageIO.write(img, "jpg", new File(path));
        } catch (IOException ex) {
            System.out.println("Error has occured during file writing: " + ex.getMessage());
        }
    }

    public static BufferedImage convertIconToImage(ImageIcon icon) {
        BufferedImage image = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics graphics = image.createGraphics();
        icon.paintIcon(null, graphics, 0, 0);
        graphics.dispose();
        return image;
    }

    public static BufferedImage convertImageToGreyscale(BufferedImage image, int channel) {
        int red;
        int green;
        int blue;
        int combined;

        for (int w = 0; w < image.getWidth(); w++) {
            for (int h = 0; h < image.getHeight(); h++) {
                Color c = new Color(image.getRGB(w, h));

                switch (channel) {
                    case CHANNEL_RED:
                        red = c.getRed();
                        image.setRGB(w, h, new Color(red, red, red).getRGB());
                        break;
                    case CHANNEL_GREEN:
                        green = c.getGreen();
                        image.setRGB(w, h, new Color(green, green, green).getRGB());
                        break;
                    case CHANNEL_BLUE:
                        blue = c.getBlue();
                        image.setRGB(w, h, new Color(blue, blue, blue).getRGB());
                        break;
                    case CHANNEL_COMBINED:
                        red = c.getRed();
                        green = c.getGreen();
                        blue = c.getBlue();
                        combined = (red + green + blue) / 3;
                        image.setRGB(w, h, new Color(combined, combined, combined).getRGB());
                        break;
                }
            }
        }

        return image;
    }

    public static BufferedImage applyMaskToImage(BufferedImage img, int[][]... masks) {
        BufferedImage copy = deepCopy(img);
        int boundary = masks[0].length / 2;
        int maskLength = masks[0].length;
        int[][] field;
        int newValue;

        for (int w = boundary; w < img.getWidth() - boundary; w++) {
            for (int h = boundary; h < img.getHeight() - boundary; h++) {
                field = SimpleFilter.getFieldArray(w, h, img, maskLength);
                newValue = convolutionOnePixel(maskLength, masks, field);

                copy.setRGB(w, h, newValue);
            }
        }

        return copy;
    }

    private static int convolutionOnePixel(int size, int[][][] masks, int[][] partOfImage) {
        int[] convolutedValue = new int[masks.length];
        int convolutedValueSum = 0;

        for (int j = 0; j < masks.length; j++) {
            for (int w = 0; w < size; w++) {
                for (int h = 0; h < size; h++) {
                    convolutedValue[j] += (partOfImage[w][h] * masks[j][w][h]);
                }
            }
        }

        if (masks.length > 1) {
            for (int j = 0; j < masks.length; j++) {
                convolutedValueSum += Math.abs(convolutedValue[j]);
            }
        } else {
            int sum = 0;
            for (int w = 0; w < size; w++) {
                for (int h = 0; h < size; h++) {
                    sum += masks[0][w][h];
                }
            }

            convolutedValueSum = convolutedValue[0] / (sum > 0 ? sum : 1);
        }

        return convolutedValueSum;
    }

    public static BufferedImage arithmeticalOperations(ArithmeticalOperationType arithmeticalOperationType, int value, BufferedImage image){
        double brightestValueRed = 0;
        double brightestValueGreen = 0;
        double brightestValueBlue = 0;

        for (int w = 0; w < image.getWidth(); w++) {
            for (int h = 0; h < image.getHeight(); h++) {
                Color color = new Color(image.getRGB(w, h));

                if (color.getRed() > brightestValueRed)
                    brightestValueRed = color.getRed();

                if (color.getGreen() > brightestValueGreen)
                    brightestValueGreen = color.getGreen();

                if (color.getBlue() > brightestValueBlue)
                    brightestValueBlue = color.getBlue();
            }
        }

        double normalizationFactorRed;
        double normalizationFactorGreen;
        double normalizationFactorBlue;

        switch (arithmeticalOperationType){
            case ADDITION:
                normalizationFactorRed = 255.0 / (brightestValueRed + value);
                normalizationFactorGreen = 255.0 / (brightestValueGreen + value);
                normalizationFactorBlue = 255.0 / (brightestValueBlue + value);

                for (int i = 0; i < 256; i++) {
                    int darkenedValueRed = (int) (normalizationFactorRed * (i + value));
                    int darkenedValueGreen = (int) (normalizationFactorGreen * (i + value));
                    int darkenedValueBlue = (int) (normalizationFactorBlue * (i + value));

                    LOOKUP_TABLE_RED[i] = darkenedValueRed;
                    LOOKUP_TABLE_GREEN[i] = darkenedValueGreen;
                    LOOKUP_TABLE_BLUE[i] = darkenedValueBlue;
                }
                break;
            case SUBTRACTION:
                normalizationFactorRed = 255.0 / (brightestValueRed - value);
                normalizationFactorGreen = 255.0 / (brightestValueGreen - value);
                normalizationFactorBlue = 255.0 / (brightestValueBlue - value);

                for (int i = 0; i < 256; i++) {
                    int darkenedValueRed = (int) (normalizationFactorRed * (i - value));
                    int darkenedValueGreen = (int) (normalizationFactorGreen * (i - value));
                    int darkenedValueBlue = (int) (normalizationFactorBlue * (i - value));

                    LOOKUP_TABLE_RED[i] = darkenedValueRed;
                    LOOKUP_TABLE_GREEN[i] = darkenedValueGreen;
                    LOOKUP_TABLE_BLUE[i] = darkenedValueBlue;
                }
                break;
            case MULTIPLICATION:
                normalizationFactorRed = 255.0 / (brightestValueRed * value);
                normalizationFactorGreen = 255.0 / (brightestValueGreen * value);
                normalizationFactorBlue = 255.0 / (brightestValueBlue * value);

                for (int i = 0; i < 256; i++) {
                    int darkenedValueRed = (int) (normalizationFactorRed * (i * value));
                    int darkenedValueGreen = (int) (normalizationFactorGreen * (i * value));
                    int darkenedValueBlue = (int) (normalizationFactorBlue * (i * value));

                    LOOKUP_TABLE_RED[i] = darkenedValueRed;
                    LOOKUP_TABLE_GREEN[i] = darkenedValueGreen;
                    LOOKUP_TABLE_BLUE[i] = darkenedValueBlue;
                }
                break;
            case DIVISION:
                normalizationFactorRed = 255.0 / (brightestValueRed / value);
                normalizationFactorGreen = 255.0 / (brightestValueGreen / value);
                normalizationFactorBlue = 255.0 / (brightestValueBlue / value);

                for (int i = 0; i < 256; i++) {
                    int darkenedValueRed = (int) (normalizationFactorRed * (i / value));
                    int darkenedValueGreen = (int) (normalizationFactorGreen * (i / value));
                    int darkenedValueBlue = (int) (normalizationFactorBlue * (i / value));

                    LOOKUP_TABLE_RED[i] = darkenedValueRed;
                    LOOKUP_TABLE_GREEN[i] = darkenedValueGreen;
                    LOOKUP_TABLE_BLUE[i] = darkenedValueBlue;
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + arithmeticalOperationType);
        }

        java.util.List<int[]> lookupTables = Arrays.asList(LOOKUP_TABLE_RED, LOOKUP_TABLE_GREEN, LOOKUP_TABLE_BLUE);

        return applyLutAtImage(image, lookupTables);
    }

    public static BufferedImage changeBrightness(BufferedImage image, double lvl) {
        int brightestValueRed = 0;
        int brightestValueGreen = 0;
        int brightestValueBlue = 0;

        for (int w = 0; w < image.getWidth(); w++) {
            for (int h = 0; h < image.getHeight(); h++) {
                Color color = new Color(image.getRGB(w, h));

                if (color.getRed() > brightestValueRed)
                    brightestValueRed = color.getRed();

                if (color.getGreen() > brightestValueGreen)
                    brightestValueGreen = color.getGreen();

                if (color.getBlue() > brightestValueBlue)
                    brightestValueBlue = color.getBlue();
            }
        }

        double normalizationFactorRed = 255 / Math.pow(brightestValueRed, lvl);
        double normalizationFactorGreen = 255 / Math.pow(brightestValueGreen, lvl);
        double normalizationFactorBlue = 255 / Math.pow(brightestValueBlue, lvl);

        for (int i = 0; i < 256; i++) {
            int darkenedValueRed = (int) (normalizationFactorRed * Math.pow(i, lvl));
            int darkenedValueGreen = (int) (normalizationFactorGreen * Math.pow(i, lvl));
            int darkenedValueBlue = (int) (normalizationFactorBlue * Math.pow(i, lvl));

            LOOKUP_TABLE_RED[i] = darkenedValueRed;
            LOOKUP_TABLE_GREEN[i] = darkenedValueGreen;
            LOOKUP_TABLE_BLUE[i] = darkenedValueBlue;
        }

        java.util.List<int[]> lookupTables = Arrays.asList(LOOKUP_TABLE_RED, LOOKUP_TABLE_GREEN, LOOKUP_TABLE_BLUE);

        return applyLutAtImage(image, lookupTables);
    }

    public static BufferedImage applyLutAtImage(BufferedImage image, List<int[]> lookupTables) {
        int[] lookupTableRed = lookupTables.get(0);
        int[] lookupTableGreen = lookupTables.get(1);
        int[] lookupTableBlue = lookupTables.get(2);

        for (int w = 0; w < image.getWidth(); w++) {
            for (int h = 0; h < image.getHeight(); h++) {
                Color color = new Color(image.getRGB(w, h));

                int red = lookupTableRed[color.getRed()];
                int green = lookupTableGreen[color.getGreen()];
                int blue = lookupTableBlue[color.getBlue()];

                Color newColor = new Color(red, green, blue);

                image.setRGB(w, h, newColor.getRGB());
            }
        }
        return image;
    }

}
