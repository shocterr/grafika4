package pl.edu.pb.wi.filtration;

import pl.edu.pb.wi.shered.ImageSharedOperations;

import java.awt.image.BufferedImage;

public class GaussFilter {

    private static final int[][] MASK = {
            {1, 2, 1},
            {2, 4, 2},
            {1, 2, 1}
    };

    public static BufferedImage applyFilter(BufferedImage image){
        return ImageSharedOperations.applyMaskToImage(image, MASK);
    }

}
