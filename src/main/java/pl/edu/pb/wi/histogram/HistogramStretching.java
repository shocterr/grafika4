package pl.edu.pb.wi.histogram;

import pl.edu.pb.wi.shered.ImageSharedOperations;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;

public class HistogramStretching {

    public static BufferedImage stretch(BufferedImage image) {
        int[] lookupTableRed = new int[256];
        int[] lookupTableGreen = new int[256];
        int[] lookupTableBlue = new int[256];

        double redMin = Double.MAX_VALUE;
        double greenMin = Double.MAX_VALUE;
        double blueMin = Double.MAX_VALUE;
        double redMax = Double.MIN_VALUE;
        double greenMax = Double.MIN_VALUE;
        double blueMax = Double.MIN_VALUE;

        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                Color color = new Color(image.getRGB(i, j));

                if (color.getRed() < redMin)
                    redMin = color.getRed();

                if (color.getGreen() < greenMin)
                    greenMin = color.getGreen();

                if (color.getBlue() < blueMin)
                    blueMin = color.getBlue();

                if (color.getRed() > redMax)
                    redMax = color.getRed();

                if (color.getGreen() > greenMax)
                    greenMax = color.getGreen();

                if (color.getBlue() > blueMax)
                    blueMax = color.getBlue();
            }
        }

        for (int i = 0; i < 256; i++) {
                int modifiedRed = (int) ((i - redMin) * 255 / (redMax - redMin));
                int modifiedGreen = (int) ((i - greenMin) * 255 / (greenMax - greenMin));
                int modifiedBlue = (int) ((i - blueMin) * 255 / (blueMax - blueMin));

                lookupTableRed[i] = modifiedRed;
                lookupTableGreen[i] = modifiedGreen;
                lookupTableBlue[i] = modifiedBlue;
            }

            List<int[]> lookupTables = Arrays.asList(lookupTableRed, lookupTableGreen, lookupTableBlue);

            return ImageSharedOperations.applyLutAtImage(image, lookupTables);
        }

    }
