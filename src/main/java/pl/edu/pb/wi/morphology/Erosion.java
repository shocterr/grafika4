package pl.edu.pb.wi.morphology;

import pl.edu.pb.wi.filtration.SimpleFilter;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class Erosion {

    private static final int[][] kernel = {
            {255, 255, 255},
            {255, 255, 255},
            {255, 255, 255}
    };

    public static BufferedImage erosion(BufferedImage image) {
        int boundary = kernel.length / 2;
        BufferedImage imageCopy = SimpleFilter.deepCopy(image);
        int[][] field;

        for (int w = boundary; w < image.getWidth() - boundary; w++) {
            for (int h = boundary; h < image.getHeight() - boundary; h++) {
                field = SimpleFilter.getFieldArray(w, h, image, 3);
                imageCopy.setRGB(w, h, pixelToBackground(field) ? Color.WHITE.getRGB() : image.getRGB(w, h));
            }
        }

        return imageCopy;
    }


    private static boolean pixelToBackground(int[][] field) {
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                if (field[i][j] == kernel[i][j]) {
                    return true;
                }
            }
        }
        return false;
    }

}
