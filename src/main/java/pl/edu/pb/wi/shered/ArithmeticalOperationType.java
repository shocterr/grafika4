package pl.edu.pb.wi.shered;

public enum ArithmeticalOperationType {

    ADDITION, SUBTRACTION, MULTIPLICATION, DIVISION

}
