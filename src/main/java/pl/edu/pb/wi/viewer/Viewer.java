package pl.edu.pb.wi.viewer;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.DefaultDrawingSupplier;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.xy.DefaultXYDataset;
import org.jfree.data.xy.XYBarDataset;
import pl.edu.pb.wi.binarization.Binarization;
import pl.edu.pb.wi.filtration.*;
import pl.edu.pb.wi.histogram.HistogramEquation;
import pl.edu.pb.wi.histogram.HistogramStretching;
import pl.edu.pb.wi.morphology.*;
import pl.edu.pb.wi.shered.ArithmeticalOperationType;
import pl.edu.pb.wi.shered.ImageSharedOperations;
import pl.edu.pb.wi.histogram.HistogramOperations;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;

public class Viewer extends JFrame {

    private final JMenuBar menuBar = new JMenuBar();
    private final JMenu files = new JMenu("File");
    private final JMenu histogram = new JMenu("Histograms");
    private final JMenu filter = new JMenu("Filter");
    private final JMenu binarization = new JMenu("Binarization");
    private final JMenu morphology = new JMenu("Morphology");
    private final JMenuItem loadImage = new JMenuItem("Load image");
    private final JMenuItem saveImage = new JMenuItem("Save image");
    private final JMenuItem originalImgMenuItem = new JMenuItem("Original image");
    private final JMenuItem brightnessLvl = new JMenuItem("Change image brightness ");
    private final JMenuItem calculateHistograms = new JMenuItem("Calculate histograms");
    private final JMenuItem showHistograms = new JMenuItem("Show histograms");
    private final JMenuItem equateHistogram = new JMenuItem("Equate Histogram");
    private final JMenuItem stretchHistogram = new JMenuItem("Stretch Histogram");
    private final JMenuItem filterImage = new JMenuItem("Filter image");
    private final JMenuItem medianFilter = new JMenuItem("Median filter");
    private final JMenuItem sobelFilter = new JMenuItem("Sobel filter");
    private final JMenuItem highPassFilter = new JMenuItem("High pass filter");
    private final JMenuItem gaussFilter = new JMenuItem("Gauss filter");
    private final JMenuItem convertToGreyscaleRed = new JMenuItem("Convent image to greyscale based on red channel");
    private final JMenuItem convertToGreyscaleGreen = new JMenuItem("Convent image to greyscale based on green channel");
    private final JMenuItem convertToGreyscaleBlue = new JMenuItem("Convent image to greyscale based on blue channel");
    private final JMenuItem convertToGreyscaleCombined = new JMenuItem("Convent image to greyscale based on combined channels");
    private final JMenuItem addition = new JMenuItem("Addition");
    private final JMenuItem subtraction = new JMenuItem("Subtraction");
    private final JMenuItem multiplication = new JMenuItem("Multiplication");
    private final JMenuItem division = new JMenuItem("Division");
    private final JMenuItem manualBinarization = new JMenuItem("Manual binarization");
    private final JMenuItem procentageBinarization = new JMenuItem("Procentage Binarization");
    private final JMenuItem dilatation = new JMenuItem("Dilatation");
    private final JMenuItem erosion = new JMenuItem("Erosion");
    private final JMenuItem opening = new JMenuItem("Opening");
    private final JMenuItem closing = new JMenuItem("Closing");
    private final JMenuItem hitOrMiss = new JMenuItem("Hit or miss");
    private final JLabel imageLabel = new JLabel();

    private BufferedImage originalImg;

    public Viewer() {
        this.setLayout(new BorderLayout());
        this.setTitle("Podstawy Biometrii");
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);

        this.menuBar.add(this.files);
        this.menuBar.add(this.histogram);
        this.menuBar.add(this.filter);
        this.menuBar.add(binarization);
        this.menuBar.add(this.morphology);
        this.morphology.add(this.dilatation);
        this.morphology.add(this.erosion);
        this.morphology.add(this.opening);
        this.morphology.add(this.closing);
        this.morphology.add(this.hitOrMiss);
        this.binarization.add(manualBinarization);
        this.binarization.add(procentageBinarization);
        this.files.add(this.loadImage);
        this.files.add(this.saveImage);
        this.files.add(this.originalImgMenuItem);
        this.files.add(this.convertToGreyscaleRed);
        this.files.add(this.convertToGreyscaleGreen);
        this.files.add(this.convertToGreyscaleBlue);
        this.files.add(this.convertToGreyscaleCombined);
        this.files.add(this.addition);
        this.files.add(this.subtraction);
        this.files.add(this.multiplication);
        this.files.add(this.division);
        this.histogram.add(this.calculateHistograms);
        this.histogram.add(this.brightnessLvl);
        this.histogram.add(this.equateHistogram);
        this.histogram.add(this.showHistograms);
        this.histogram.add(this.stretchHistogram);
        this.filter.add(this.filterImage);
        this.filter.add(this.medianFilter);
        this.filter.add(this.sobelFilter);
        this.filter.add(this.highPassFilter);
        this.filter.add(this.gaussFilter);

        this.add(this.menuBar, BorderLayout.NORTH);
        this.add(this.imageLabel, BorderLayout.CENTER);
        this.imageLabel.setHorizontalAlignment(JLabel.CENTER);
        this.imageLabel.setVerticalAlignment(JLabel.CENTER);

        this.loadImage.addActionListener((ActionEvent e) -> {
            JFileChooser imageOpener = new JFileChooser();
            imageOpener.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    String fileName = f.getName().toLowerCase();
                    if (fileName.endsWith(".jpg") || fileName.endsWith(".png")
                            || fileName.endsWith(".tiff") || fileName.endsWith(".jpeg")) {
                        return true;
                    } else return false;
                }

                @Override
                public String getDescription() {
                    return "Image files (.jpg, .png, .tiff)";
                }
            });

            int returnValue = imageOpener.showDialog(null, "Select image");
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                BufferedImage img = ImageSharedOperations.loadImage(imageOpener.getSelectedFile().getPath());
                originalImg = ImageSharedOperations.loadImage(imageOpener.getSelectedFile().getPath());
                this.imageLabel.setIcon(new ImageIcon(img));
            }
        });

        this.saveImage.addActionListener((ActionEvent e) -> {
            String path = "./image.jpg";
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            ImageSharedOperations.saveImage(img, path);
        });

        this.calculateHistograms.addActionListener((ActionEvent e) -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            HistogramOperations.calculateHistograms(img);
        });

        this.convertToGreyscaleRed.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage convertedImage = ImageSharedOperations.convertImageToGreyscale(img,
                    ImageSharedOperations.CHANNEL_RED);
            imageLabel.setIcon(new ImageIcon(convertedImage));
        });

        this.convertToGreyscaleGreen.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage convertedImage = ImageSharedOperations.convertImageToGreyscale(img,
                    ImageSharedOperations.CHANNEL_GREEN);
            imageLabel.setIcon(new ImageIcon(convertedImage));
        });

        this.convertToGreyscaleBlue.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage convertedImage = ImageSharedOperations.convertImageToGreyscale(img,
                    ImageSharedOperations.CHANNEL_BLUE);
            imageLabel.setIcon(new ImageIcon(convertedImage));
        });

        this.convertToGreyscaleCombined.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage convertedImage = ImageSharedOperations.convertImageToGreyscale(img,
                    ImageSharedOperations.CHANNEL_COMBINED);
            imageLabel.setIcon(new ImageIcon(convertedImage));
        });

        this.filterImage.addActionListener((ActionEvent e) -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            int size = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter size of filter (odd values)"));
            BufferedImage result = SimpleFilter.filterImage(img, size);
            this.imageLabel.setIcon(new ImageIcon(result));
        });

        this.medianFilter.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            int size = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter size of filter (odd values)"));
            BufferedImage result = MedianFilter.filterImage(img, size);
            this.imageLabel.setIcon(new ImageIcon(result));
        });

        this.sobelFilter.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage result = SobelFilter.applySobelFilter(img);
            this.imageLabel.setIcon(new ImageIcon(result));
        });

        this.highPassFilter.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage result = HighPassFilter.applyFilter(img);
            this.imageLabel.setIcon(new ImageIcon(result));
        });

        this.gaussFilter.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            BufferedImage result = GaussFilter.applyFilter(img);
            this.imageLabel.setIcon(new ImageIcon(result));
        });

        this.brightnessLvl.addActionListener(e -> {
            BufferedImage image = ImageSharedOperations.convertIconToImage((ImageIcon) imageLabel.getIcon());
            double lvl = Double.parseDouble(JOptionPane.showInputDialog(this,
                    "Enter lvl: <1 brighter, >1 darker"));
            BufferedImage darkenedImage = ImageSharedOperations.changeBrightness(image, lvl);
            imageLabel.setIcon(new ImageIcon(darkenedImage));
        });

        this.originalImgMenuItem.addActionListener(e -> imageLabel.setIcon(new ImageIcon(originalImg)));

        this.addition.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            int value = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter value"));
            BufferedImage result = ImageSharedOperations.arithmeticalOperations(ArithmeticalOperationType.ADDITION, value, img);
            this.imageLabel.setIcon(new ImageIcon(result));
        });

        this.subtraction.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            int value = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter value"));
            BufferedImage result = ImageSharedOperations.arithmeticalOperations(ArithmeticalOperationType.SUBTRACTION, value, img);
            this.imageLabel.setIcon(new ImageIcon(result));
        });

        this.multiplication.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            int value = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter value"));
            BufferedImage result = ImageSharedOperations.arithmeticalOperations(ArithmeticalOperationType.MULTIPLICATION, value, img);
            this.imageLabel.setIcon(new ImageIcon(result));
        });

        this.division.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());
            int value = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter value"));
            BufferedImage result = ImageSharedOperations.arithmeticalOperations(ArithmeticalOperationType.DIVISION, value, img);
            this.imageLabel.setIcon(new ImageIcon(result));
        });

        this.equateHistogram.addActionListener(e -> {
            BufferedImage image = ImageSharedOperations.convertIconToImage((ImageIcon) imageLabel.getIcon());

            BufferedImage modifiedImage = HistogramEquation.equate(image);
            imageLabel.setIcon(new ImageIcon(modifiedImage));

            List<double[]> equatedHistograms = HistogramOperations.calculateHistograms(modifiedImage);
            showHistogram(equatedHistograms);
        });

        this.stretchHistogram.addActionListener(e -> {
            BufferedImage image = ImageSharedOperations.convertIconToImage((ImageIcon) imageLabel.getIcon());

            BufferedImage modifiedImage = HistogramStretching.stretch(image);
            imageLabel.setIcon(new ImageIcon(modifiedImage));

            List<double[]> equatedHistograms = HistogramOperations.calculateHistograms(modifiedImage);
            showHistogram(equatedHistograms);
        });

        this.showHistograms.addActionListener(e -> {
            BufferedImage image = ImageSharedOperations.convertIconToImage((ImageIcon) imageLabel.getIcon());
            List<double[]> histograms = HistogramOperations.calculateHistograms(image);

            showHistogram(histograms);

        });

        this.manualBinarization.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());

            int thresholdParameter = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter threshold parameter"));

            BufferedImage binarizedImage = Binarization.manualBinarization(img, thresholdParameter);
            imageLabel.setIcon(new ImageIcon(binarizedImage));
        });

        this.procentageBinarization.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());

            int percentage = Integer.parseInt(JOptionPane.showInputDialog(this,
                    "Enter percentage of black"));

            BufferedImage binarizedImage = Binarization.percentageBinarization(img, percentage);
            imageLabel.setIcon(new ImageIcon(binarizedImage));
        });

        this.dilatation.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());

            BufferedImage dilatatedImage = Dilatation.dilatate(img);
            imageLabel.setIcon(new ImageIcon(dilatatedImage));
        });

        this.erosion.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());

            BufferedImage erosionImage = Erosion.erosion(img);
            imageLabel.setIcon(new ImageIcon(erosionImage));
        });

        this.opening.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());

            BufferedImage openedImage = Opening.opening(img);
            imageLabel.setIcon(new ImageIcon(openedImage));
        });

        this.closing.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());

            BufferedImage closedImage = Closing.closing(img);
            imageLabel.setIcon(new ImageIcon(closedImage));
        });

        this.hitOrMiss.addActionListener(e -> {
            BufferedImage img = ImageSharedOperations.convertIconToImage((ImageIcon) this.imageLabel.getIcon());

            BufferedImage hitOrMissImage = HitOrMiss.performHitORMiss(img);
            imageLabel.setIcon(new ImageIcon(hitOrMissImage));
        });

    }

    private void showHistogram(List<double[]> histograms) {
        double[] x = new double[256];
        for (int i = 0; i < 256; i++) x[i] = i;

        double[][] seriesRed = new double[2][];
        double[][] seriesBlue = new double[2][];
        double[][] seriesGreen = new double[2][];
        double[][] seriesCombined = new double[2][];
        seriesRed[0] = x;
        seriesRed[1] = histograms.get(0);
        seriesBlue[0] = x;
        seriesBlue[1] = histograms.get(1);
        seriesGreen[0] = x;
        seriesGreen[1] = histograms.get(2);
        seriesCombined[0] = x;
        seriesCombined[1] = histograms.get(3);

        DefaultXYDataset dataset = new DefaultXYDataset();
        dataset.addSeries("Red", seriesRed);
        dataset.addSeries("Green", seriesGreen);
        dataset.addSeries("Blue", seriesBlue);
        dataset.addSeries("Combined", seriesCombined);

        XYBarDataset barDataset = new XYBarDataset(dataset,0.8);


        JFreeChart chart = ChartFactory.createXYBarChart(null, "colors", false, "count", barDataset,
                PlotOrientation.VERTICAL, true, true, false);

        XYPlot plot = (XYPlot) chart.getPlot();
        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setBarPainter(new StandardXYBarPainter());

        Paint[] paintArray = {
                new Color(0x80ff0000, true),
                new Color(0x8000ff00, true),
                new Color(0x800000ff, true),
                new Color(0xB6343434, true)
        };

        plot.setDrawingSupplier(new DefaultDrawingSupplier(
                paintArray,
                DefaultDrawingSupplier.DEFAULT_FILL_PAINT_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_OUTLINE_PAINT_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_STROKE_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_OUTLINE_STROKE_SEQUENCE,
                DefaultDrawingSupplier.DEFAULT_SHAPE_SEQUENCE));

        ChartFrame frame = new ChartFrame("Histograms", chart);
        frame.pack();
        frame.setVisible(true);
    }

}
