package pl.edu.pb.wi.filtration;

import pl.edu.pb.wi.shered.ImageSharedOperations;

import java.awt.image.BufferedImage;

public class SobelFilter {

    private static final int[][] MASK_X = {
            {-1, 0, 1},
            {-2, 0, 2},
            {-1, 0, 1}
    };

    private static final int[][] MASK_Y = {
            {1, 2, 1},
            {0, 0, 0},
            {-1, -2, -1}
    };

    public static BufferedImage applySobelFilter(BufferedImage image){
        return ImageSharedOperations.applyMaskToImage(image, MASK_Y, MASK_X);
    }

}
