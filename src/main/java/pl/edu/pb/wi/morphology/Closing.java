package pl.edu.pb.wi.morphology;

import java.awt.image.BufferedImage;

public abstract class Closing {

    public static BufferedImage closing(BufferedImage image){
        BufferedImage imgCopy = Dilatation.dilatate(image);
        return Erosion.erosion(imgCopy);
    }

}
