package pl.edu.pb.wi.filtration;

import pl.edu.pb.wi.shered.ImageSharedOperations;

import java.awt.image.BufferedImage;

public class HighPassFilter {

    private static final int[][] MASK_HP3 = {
            {-1, -1, -1},
            {-1, 9, -1},
            {-1, -1, -1}
    };

    public static BufferedImage applyFilter(BufferedImage image){
        return ImageSharedOperations.applyMaskToImage(image, MASK_HP3);
    }

}
