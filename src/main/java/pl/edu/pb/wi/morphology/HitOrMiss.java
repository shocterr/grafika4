package pl.edu.pb.wi.morphology;

import pl.edu.pb.wi.filtration.SimpleFilter;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class HitOrMiss {

    private static final int[][] kernel = {
            {8, 0, 8},
            {255, 0, 0},
            {255, 255, 8}
    };
    private static final int[][] kernel1 = {
            {8, 0, 8},
            {0, 0, 255},
            {8, 255, 255}
    };
    private static final int[][] kernel2 = {
            {255, 255, 8},
            {255, 0, 0},
            {8, 0, 8}
    };
    private static final int[][] kernel3 = {
            {8, 255, 255},
            {0, 0, 255},
            {8, 0, 8}
    };

    public static BufferedImage performHitORMiss(BufferedImage image) {
        int boundary = kernel.length / 2;
        BufferedImage imageCopy = new BufferedImage(image.getWidth(), image.getHeight(), image.getType());
        int[][] field;

        for (int w = boundary; w < image.getWidth() - boundary; w++) {
            for (int h = boundary; h < image.getHeight() - boundary; h++) {
                field = SimpleFilter.getFieldArray(w, h, image, 3);
                imageCopy.setRGB(w, h, pixelToForeground(field) ? Color.BLACK.getRGB() : Color.WHITE.getRGB());
            }
        }

        return imageCopy;
    }


    private static boolean pixelToForeground(int[][] field) {
        //Piece of shit which works
        return (kernel[0][1] == field[0][1] && kernel[1][1] == field[1][1] && kernel[1][2] == field[1][2] &&
                        kernel[1][0] == field[1][0] && kernel[2][0] == field[2][0] && kernel[2][1] == field[2][1]) ||

                (kernel1[0][1] == field[0][1] && kernel1[1][1] == field[1][1] && kernel1[1][0] == field[1][0] &&
                        kernel1[1][2] == field[1][2] && kernel1[2][1] == field[2][1] && kernel1[2][2] == field[2][2]) ||

                (kernel2[1][2] == field[1][2] && kernel2[1][1] == field[1][1] && kernel2[2][1] == field[2][1] &&
                        kernel2[0][0] == field[0][0] && kernel2[0][1] == field[0][1] && kernel2[1][0] == field[1][0]) ||

                (kernel3[1][0] == field[1][0] && kernel3[1][1] == field[1][1] && kernel3[2][1] == field[2][1] &&
                        kernel3[0][1] == field[0][1] && kernel3[0][2] == field[0][2] && kernel3[1][2] == field[1][2]);
    }

}
