package pl.edu.pb.wi.binarization;

import pl.edu.pb.wi.histogram.HistogramOperations;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class Binarization {

    public static BufferedImage manualBinarization(BufferedImage img, int threshold) {
        for (int w = 0; w < img.getWidth(); w++) {
            for (int h = 0; h < img.getHeight(); h++) {
                binarizePixel(img, w, h, threshold);
            }
        }
        return img;
    }

    static void binarizePixel(BufferedImage imageCopy, int w, int h, double threshold) {
        Color c = new Color(imageCopy.getRGB(w, h));
        imageCopy.setRGB(w, h,
                c.getRed() >= threshold ? Color.BLACK.getRGB() : Color.WHITE.getRGB());
    }

    public static BufferedImage percentageBinarization(BufferedImage image, int percentage) {
        double[] histogram = HistogramOperations.calculateHistograms(image).get(0);

        double asd = image.getHeight() * image.getWidth() * (double) percentage / 10000;

        int threshold = 0;

        for (int i = 0; i < histogram.length; i++) {
            if (histogram[i] >= asd) {
                threshold = i;
                break;
            }
        }

        return manualBinarization(image, threshold);
    }

}
